use <cherry-mx.scad>;
use <utils.scad>;

include <JC33V-config.scad>;

fingerLayout = [
	// Left
	[0.00, 0.00], [0.00, 1.00], [0.00, 2.00],
	[1.00, 0.25], [1.00, 1.25], [1.00, 2.25],
	[2.00, 0.50], [2.00, 1.50], [2.00, 2.50],
	              [3.00, 1.25], [3.00, 2.25],
								[4.00, 1.25], [4.00, 2.25],
];

thumbLayout = [
	// Left
	[1.00, 0.75],
	[2.00, 1.00],
	[3.00, 1.25],
	[4.00, 1.25],
];

module plate(layout) {
	$fn = 50;
	
	//
	// Main plate
	//
	difference() {
		cube([plateWidth, plateDepth, plateHeight]);
		
		translate([plateFrontWidth, -1, -plateHeight]) {
			cube([100, 25 + 1, plateHeight * 3.0]);
		}
		
		for (key = layout) {
			swHole(key[0], key[1]);
		}
	}

	translate([0, 0, -5.0]) {
		for (key = layout) {
			x = swX(key[1]) - 3.75;
			y = swY(key[0]) - 3.75;
			
			reinforce(y, x, 19 + 2.5, 19 + 2.5, 5.0);
		}
	}
	
	//
	// Plate Rim
	//
	
	translate([0, 0, -overallHeight]) {
		// Front Left
		difference() {
			cube([plateFrontWidth, 2.5, overallHeight]);
			translate([30, 4, 3.5])
				rotate([90, 0, 0])
					cylinder(r=1.0, h=5);
			translate([55.5, 4, 3.5])
				rotate([90, 0, 0])
					cylinder(r=1.0, h=5);
		}
		
		// Left
		cube([2.5, plateDepth, overallHeight]);
		
		// Rear
		translate([0, plateDepth - 2.5, 0]) {
			difference() {
				cube([plateWidth, 2.5, overallHeight]);
				translate([30, 4, 3.5])
					rotate([90, 0, 0])
						cylinder(r=1.0, h=5);
				translate([65, 4, 3.5])
					rotate([90, 0, 0])
						cylinder(r=1.0, h=5);
			}
		}
		
		// Right Front
		translate([plateFrontWidth - 2.5, 0, 0]) {
			cube([2.5, 25, overallHeight]);
		}
		
		// Front Right
		translate([plateFrontWidth - 2.5, 25, 0])
				cube([plateWidth - plateFrontWidth + 2, 2.5,  overallHeight]);
		
		// Right Rear
		translate([plateWidth - 2.5, 25, 0])
			cube([2.5, plateDepth - 25, overallHeight]);
	}
}

module leftPlate() {
	plate(fingerLayout);
}

module rightPlate() {
	mirror()
		leftPlate();
}

module leftThumbPlate() {
	plate(thumbLayout);
	translate([39, 0, -5.0])
		cube([3.0, plateDepth, 5.0]);
}

module rightThumbPlate() {
	mirror()
		leftThumbPlate();
}

rotate([0, 180, 180])
	leftPlate();
translate([0, 10, 0])
	rotate([0, 180, 0])
		rightPlate();

/*
rotate([0, 180, 180])
	leftThumbPlate();
translate([0, 10, 0])
	rotate([0, 180, 0])
		rightThumbPlate();

*/