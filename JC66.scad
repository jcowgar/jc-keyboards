use <cherry-mx.scad>;
use <teensy.scad>;
use <utils.scad>;

layout = [
	[ 0.00, [ 0.00, 1.00, 2.00, 3.00, 4.00, 5.00, 6.00 ]],
	[ 1.00, [ 0.00, 1.00, 2.00, 3.00, 4.00, 5.00, 6.00 ]],
	[ 2.00, [ 0.00, 1.00, 2.00, 3.00, 4.00, 5.00, 6.00 ]],
	[ 3.00, [ 0.00 + (1.0 / 2), 2.00, 3.00, 4.00, 5.00, 6.00 ]],
	[ 4.00, [ 1.25 + (0.25 / 2), 2.5 + (.25 / 2), 4.00, 5 + (.75 / 2), ]],
	[ 4.25, [ 6.75 ]],
];

maxRow = nestedMax(6.75);
maxCol = 4.25;

width = swX(maxCol) + swX(1, 5) - 5;
height = swY(maxRow) + swX(1, 5) - 5;

module centerPlate() {
	centerHeight = 30;
	centerWidth = width - 40;
	
	difference() {
		polygon([[0, 0], [centerHeight / 2.0, centerWidth], [centerHeight, 0]]);
		swHole2D(0.16, 1);
		swHole2D(0.16, 0);
	}
}

module centerReinforcement() {
	difference() {
		centerPlate();
		
		translate([swX(0.16) - 1.0, swY(1) - 1.0, 0]) square(size=[14 + 2, 14 + 2]);
		translate([swX(0.16) - 1.0, swY(0) - 1.0, 0]) square(size=[14 + 2, 14 + 2]);
	}
}

module basePlate() {
	square(size=[width, height]);
}

module switchHoles() {
	for (col = layout) {
		for (row = col[1]) {
			swHole2D(col[0], row);
		}
	}
}

module plate() {
	difference() {
		basePlate();
		switchHoles();
	}
}

module leftPlate() {
	mirror()
		plate();
}

module switchBracing() {
	for (col = layout) {
		x = swX(col[0]) - 1.0;
		
		for (row = col[1]) {
			y = swY(row) - 1.0;
			
			translate([x, y])
				square(size=[14 + 2, 14 + 2]);
		}
	}
}

module bracingPlate() {
	difference() {
		basePlate();
		switchBracing();
	}
}

module leftBracingPlate() {
	mirror() bracingPlate();
}

module rim() {
	difference() {
		basePlate();
		translate([3.5, 3.5])
			square(size=[width - 7, height - 7]);
	}
}

module leftRim() {
	mirror() rim();
}

module left() {
	linear_extrude(height=1.5)
		leftPlate();

	color([1, 0, 0])
	translate([0, 0, 1.5])
		linear_extrude(height=3.0)
			leftBracingPlate();

	difference() {
		color([0, 1, 0])
			translate([0, 0, 4.5])
				linear_extrude(height=8.0)
					leftRim();

		translate([-5, 126, 11.0])
			color([1, 0, 1])
				rotate([0, 90, 0])
				cylinder(r=2, h=6, $fn=50);
	}
}

module right() {
	linear_extrude(height=1.5)
		plate();

	color([1, 0, 0])
	translate([0, 0, 1.5])
		linear_extrude(height=3.0)
			bracingPlate();

	difference() {
		color([0, 1, 0])
			translate([0, 0, 4.5])
				linear_extrude(height=8.0)
					rim();

		translate([-5, 126, 11.0])
			color([1, 0, 1])
				rotate([0, 90, 0])
				cylinder(r=2, h=6, $fn=50);
	}
}

module center() {
	linear_extrude(height=1.5) centerPlate();
}

module cReinforcement() {
	linear_extrude(height=3.5) centerReinforcement();
}

module keyboard() {
	difference() {
		union() {
			rotate([0, 0, 12]) left();
			translate([0, (height * 2) - 24, 0]) rotate([0, 0, 168]) right();
			translate([-133.9, 155.25, 0]) rotate([0, 0, 270]) center();
			translate([-133.9, 155.25, 1.5]) rotate([0, 0, 270]) color([0.8, 0.8, 0]) cReinforcement();
		}
		translate([-133.9, 155.25, -1]) rotate([0, 0, 270]) linear_extrude(height=20) swHole2D(0.16, 1);
		color([0.5, 0.5, 0.9]) translate([-133.9 + swX(1) - 1, 155.25 - swY(0.16) + 1, 1.5]) rotate([0, 0, 270]) linear_extrude(height=20) square(size=[14 + 2, 14 + 2]);
	}
}

module verticalBrace() {
	difference() {
		cube([100, 3, 8.0]);
		color([1, 0, 0.5]) translate([19.5, -2, 1]) rotate([0, 90, 90]) cylinder(r=1, h=8, $fn=50);
		color([1, 0, 0.5]) translate([19.5 + (19 * 1), -2, 1]) rotate([0, 90, 90]) cylinder(r=1, h=8, $fn=50);
		color([1, 0, 0.5]) translate([19.5 + (19 * 2), -2, 1]) rotate([0, 90, 90]) cylinder(r=1, h=8, $fn=50);
		color([1, 0, 0.5]) translate([19.5 + (19 * 3), -2, 1]) rotate([0, 90, 90]) cylinder(r=1, h=8, $fn=50);
		color([1, 0, 0.5]) translate([19.5 + (19 * 4), -2, 1]) rotate([0, 90, 90]) cylinder(r=1, h=8, $fn=50);
	}
}

difference() {
	keyboard();
	color([0.5, 0.5, 0]) translate([-141.5, 124, 4.6]) cube([106.0, 32.0, 8.0]);
}

color([0.2, 0.8, 0.3]) translate([-90, 160, 4.6]) rotate([0, 0, 168]) cube([15, 10, 8.0]);
color([0.2, 0.8, 0.3]) translate([-106, 118, 4.6]) rotate([0, 0, 12]) cube([15, 10, 8.0]);
color([0.2, 0.8, 0.3]) translate([-18.5, 77.75, 4.6]) rotate([0, 0, 192]) verticalBrace();
color([0.2, 0.8, 0.3]) translate([-18.5, 205.75, 4.6]) rotate([0, 0, 168]) verticalBrace();