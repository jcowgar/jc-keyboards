use <cherry-mx.scad>;
use <teensy.scad>;
use <utils.scad>;

layout = [
	[ 0.00, [ 0.00, 1.00, 2.00, 3.00, 4.00, 5.00, 6.00 ]],
	[ 1.00, [ 0.00, 1.00, 2.00, 3.00, 4.00, 5.00, 6.00 ]],
	[ 2.00, [ 0.00, 1.00, 2.00, 3.00, 4.00, 5.00, 6.00 ]],
	[ 2.50, [ 7.00 ]],
	[ 3.00, [ 0.00 + (1.0 / 2), 2.00, 3.00, 4.00, 5.00, 6.00 ]],
	[ 3.50, [ 7.00 ]],
	[ 4.00, [ 1.25 + (0.25 / 2), 2.5 + (.25 / 2), 4.25, 5.25 + (.75 / 2) ]],
	[ 4.50, [ 7.00 ]]
];

maxRow = nestedMax(7.0);
maxCol = 4.50;

width = swX(maxCol) + swX(1, 5) - 5;
height = swY(maxRow) + swX(1, 5) - 5;

module basePlate() {
	square(size=[width, height]);
}

module switchHoles() {
	for (col = layout) {
		for (row = col[1]) {
			swHole2D(col[0], row);
		}
	}
}

module plate() {
	difference() {
		basePlate();
		switchHoles();
	}
}

module leftPlate() {
	mirror()
		plate();
}

module switchBracing() {
	for (col = layout) {
		x = swX(col[0]) - 1.0;

		for (row = col[1]) {
			y = swY(row) - 1.0;

			translate([x, y])
				square(size=[14 + 2, 14 + 2]);
		}
	}
}

module bracingPlate() {
	difference() {
		basePlate();
		switchBracing();
	}
}

module leftBracingPlate() {
	mirror() bracingPlate();
}

module rim() {
	difference() {
		basePlate();
		translate([3.5, 3.5])
			square(size=[width - 7, height - 7]);
	}
}

module leftRim() {
	mirror() rim();
}

module left() {
	linear_extrude(height=1.5)
		leftPlate();

	color([1, 0, 0])
	translate([0, 0, 1.5])
		linear_extrude(height=3.0)
			leftBracingPlate();

	difference() {
		color([0, 1, 0])
			translate([0, 0, 4.5])
				linear_extrude(height=8.0)
					leftRim();

		translate([-5, 126, 11.0])
			color([1, 0, 1])
				rotate([0, 90, 0])
				cylinder(r=2, h=6, $fn=50);

		color([0.4, 0.9, 0.6])
			translate([-20, height - 8, 0]) cube([10, 10, 10]);
	}
}

module right() {
	linear_extrude(height=1.5)
		plate();

	color([1, 0, 0])
	translate([0, 0, 1.5])
		linear_extrude(height=3.0)
			bracingPlate();

	difference() {
		color([0, 1, 0])
			translate([0, 0, 4.5])
				linear_extrude(height=8.0)
					rim();


		translate([-1, 126, 11.0])
			color([1, 0, 1])
				rotate([0, 90, 0])
				cylinder(r=2, h=12, $fn=50);

		color([0.4, 0.9, 0.6])
			translate([10, height - 8, 0]) cube([10, 10, 10]);
	}

}

module keyboard() {
	difference() {
		union() {
			rotate([0, 0, 12]) left();
			translate([0, (height * 2) - 24, 0]) rotate([0, 0, 168]) right();
		}
	}
}

module verticalBrace() {
	difference() {
		cube([width - 5, 3, 8.0]);
		color([1, 0, 0.5]) translate([19.5, -2, 2]) rotate([0, 90, 90]) cylinder(r=2, h=8, $fn=50);
		color([1, 0, 0.5]) translate([19.5 + (19 * 1), -2, 2]) rotate([0, 90, 90]) cylinder(r=2, h=8, $fn=50);
		color([1, 0, 0.5]) translate([19.5 + (19 * 2), -2, 2]) rotate([0, 90, 90]) cylinder(r=2, h=8, $fn=50);
		color([1, 0, 0.5]) translate([19.5 + (19 * 3), -2, 2]) rotate([0, 90, 90]) cylinder(r=2, h=8, $fn=50);
		color([1, 0, 0.5]) translate([19.5 + (19 * 4), -2, 2]) rotate([0, 90, 90]) cylinder(r=2, h=8, $fn=50);
	}
}

module leftHook(height) {
	difference() {
		union() {
			cube([6, 8, height]);
			translate([6, 4, 0]) cylinder(r=4.0, h=height, $fn=50);
		}
		translate([6, 4, -1]) cylinder(r=2.0, h=height * 2.0, $fn=50);
	}

}

module fullLeft() {
	left();
	translate([0, height, 3]) rotate([0, 0, 90]) leftHook(6);
	translate([-77, 2.5, 1.5]) rotate([0, 0, 90]) verticalBrace();
}

module fullRight() {
	right();
	translate([8, height, 0]) rotate([0, 0, 90]) leftHook(3);
	translate([8, height, 9]) rotate([0, 0, 90]) leftHook(3.5);

	translate([2.5, 77, 4.5]) verticalBrace();
}

//fullRight();
fullLeft();