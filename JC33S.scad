use <cherry-mx.scad>;
use <teensy.scad>;
use <utils.scad>;

layout = [
	// Left
	[0.00, 1.00], [0.00, 2.00], [0.00, 3.00],
	[1.00, 1.25], [1.00, 2.25], [1.00, 3.25],
	[2.00, 1.50], [2.00, 2.50], [2.00, 3.50],
	[3.25, 0.25], [3.25, 1.25], [3.00, 2.25], [3.00, 3.25],
	[4.25, 0.75],
];

screwLocations = [
	[3, 21.5],
	[3, 78],
	[39.5, 64],
	[62, 40],
	[83.5, 8],
	[78.5, 83],
];

plateWidth = 200;
plateDepth = 100;

$fn=50;

module topPlate2D() {
	for (key = layout) {
		x = swX(key[0]);
		y = swY(key[1]);
		
		translate([x - 5, y - 5])
			square(size=[19 + 5, 19 + 5]);
	}
}

module switchHoles() {
	for (key = layout) {
		swHole2D(key[0], key[1], true);
	}
}

module screwHoles() {
	for (sh = screwLocations) {
		translate([sh[0], sh[1]])
			circle(screwRadius);
	}
}

module topPlate() {
	difference() {
		topPlate2D();
		switchHoles();
		screwHoles();
	}
}

//topPlate2D();
linear_extrude(height=1.5)
	topPlate();

color([1,0,0])
	linear_extrude(height=3.5)
		translate([0, 0, 1.5])
			difference() {
				topPlate2D();
				screwHoles();
				for (key = layout) {
					x = swX(key[0]);
					y = swY(key[1]);
					
					translate([x - 1.25, y - 1.25])
						square(size=[14 + 2.5, 14 + 2.5]);
				}
			}
