use <cherry-mx.scad>;
use <teensy.scad>;
use <utils.scad>;

layoutHome = [
	// Left
	[0.00, 0.00], 
	[1.00, 0.00], 
	[2.00, 0.00], 
	[3.00, 0.00],
];

layoutBack = [
	[0.00, 0.00], 
	[1.00, 0.00], 
	[2.00, 0.00], 
	[3.00, 0.00],
];

layoutCenter = [
	[3.5, 0.25], [4.00, 1.25],
	                           [5.00, 2.00],
	[6.5, 0.25], [6.00, 1.25],
];

$fn=50;

module plate(layout, padding, padding2 = [0, 0]) {
	for (key = layout) {
		x = swX(key[0]);
		y = swY(key[1]);
		
		translate([x - 5, y - 5])
			square(size=[19 + 5 + padding[0], 19 + 5 + padding[1]]);
	}
}

module switchHoles(layout, padding) {
	for (key = layout) {
		swHole2D(key[0] + padding[0], key[1] + padding[1], true);
	}
}  

module switchReinforcement(layout, padding, padding2 = [0, 0]) {
	for (key = layout) {
		x = swX(key[0]);
		y = swY(key[1]);
		
		translate([x - 1.25 + padding[0], y - 1.75 + padding[1]])
			square(size = [14 + 2.5 + padding2[0], 14 + 2.5 + padding2[1]]);
	}
} 

module centerPlate() {
	difference() {
		square(size=[222, 65]);
		switchHoles(layoutCenter, [0, 0]);
	}
}

module centerReinforcement() {
	difference() {
		plate(layoutCenter, [0, 0]);
		switchReinforcement(layoutCenter, [0, 0], [0, 0]);
	}
}

module centerPlate3D() {
	linear_extrude(height=1.5)
		centerPlate();
	translate([0, 0, 1.5])
		linear_extrude(height=3.5)
			color([1, 0, 0])
				centerReinforcement();
	translate([-2.5, 0, 0])
		cube([2.5, 65, 42]);
	translate([222, 0, 0])
		cube([2.5, 65, 42]);
	
	translate([0, 65 - 2.5, 0])
		difference() {
			cube([222, 2.5, 42]);
			translate([30, 5, 20])
				rotate([90, 0, 0])
					cylinder(r=15, h=50, $fn=50);
			translate([80, 5, 20])
				rotate([90, 0, 0])
					cylinder(r=15, h=50, $fn=50);
			translate([140, 5, 20])
				rotate([90, 0, 0])
					cylinder(r=15, h=50, $fn=50);
			translate([190, 5, 20])
				rotate([90, 0, 0])
					cylinder(r=15, h=50, $fn=50);
		}
}

module homePlate() {
	difference() {
		plate(layoutHome, [0, 15]);
		switchHoles(layoutHome, [0, 0.75]);
	}
}

module homeReinforcement() {
	difference() {
		plate(layoutHome, [0, 15], [0, 0]);
		switchReinforcement(layoutHome, [0, 15], [0, 0]);
	}
}

module homePlate3D() {
	linear_extrude(height=1.5)
		homePlate();
	translate([0, 0, 1.5])
		linear_extrude(height=3.5)
			color([1, 0, 0])
				homeReinforcement();
}

module backPlate() {
	difference() {
		plate(layoutBack, [0, 13]);
		switchHoles(layoutBack, [0, 0.65]);
	}
}

module backPlateReinforcement() {
	difference() {
		plate(layoutBack, [0, 13]);
		switchReinforcement(layoutBack, [0, 13]);
	}
}

module backPlate3D() {
	linear_extrude(height=1.5)
		backPlate();
	translate([0, 0, 1.5])
		linear_extrude(height=3.5)
			color([1, 0, 0])
				backPlateReinforcement();
}

module keyWell() {
	homePlate3D();
	translate([81, 5.0, 0])
		rotate([-90, 0, 180])
			backPlate3D();
	translate([0, 5, -3.5])
		cube([81, 3.5, 3.5]);
}

translate([0, -5, 5])
	rotate([180, 0, 0])
		keyWell();

translate([81 + 60, -5, 5])
	rotate([180, 0, 0])
		keyWell();
centerPlate3D();