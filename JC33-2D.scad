use <cherry-mx.scad>;
use <teensy.scad>;
use <utils.scad>;

include <JC33-config.scad>;

$fn=50;

module topPlate2D() {
	square(size=[plateWidth, plateDepth]);
}

module switchHoles() {
	for (key = layout) {
		swHole2D(key[0], key[1], true);
	}
}

module screwHoles() {
	for (sh = screwLocations) {
		translate([sh[0], sh[1]])
			circle(screwRadius);
	}
}

module topPlate() {
	difference() {
		topPlate2D();
		switchHoles();
		screwHoles();
	}
}

topPlate();
