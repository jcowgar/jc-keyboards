include <JC33V-config.scad>;

$fn=50;

size = 2.5;

module upright(height, hole2) {
	difference() {
		cube([10, 2.5, height]);
		translate([5, size * 2.0, 30])
			rotate([90, 0, 0])
				cylinder(r=1, h=size * 3);
		translate([5, size * 2.0, hole2])
			rotate([90, 0, 0])
				cylinder(r=1, h=size * 3);
	}
}

module frontLeg() {
	upright(plateFrontWidth, 55);
	translate([10, 0, 0])
		upright(plateFrontWidth, 55);
	cube([40, size, 10]);
}

module rearLeg() {
	upright(70, 65);
	translate([10, 0, 0])
		upright(70, 65);
	cube([40, size, 25]);
	cube([60, size, 15]);
}

rotate([90, 0, 0])
	//frontLeg();
	rearLeg();
