use <cherry-mx.scad>;
use <teensy.scad>;
use <utils.scad>;

layout = [
[ 0.25, 1.25, 2.25, 3.25, 4.25, 5.25, 6.25, 7.25, 8.25, 9.25, 10.25, 11.25, 12.25, ],
[ 0.25, 1.50, 2.50, 3.50, 4.50, 5.50, 6.50, 7.50, 8.50, 9.50, 10.50, 11.50, 12.50, ],
[ 0.25 - (0.25 / 2), 1.25, 2.25, 3.25, 4.25, 5.25, 6.75, 7.75, 8.75, 9.75, 10.75, 11.75 + (0.75 / 2), ],
[ 0.00, 1.00, 2.00, 3.00, 4.00, 5.00, 6.00, 7.00, 8.00, 9.00, 10.00, 11.00, 12.00 + (.5 / 2) ],
[ 0.50 + (.25 / 2), 1.75 + (.25 / 2), 3 + (.25 / 2), 4.25 + (1 / 2), 6.75 + (1 / 2), 8.75 + (.25 / 2),  10.00 + (.25 / 2), 11.25 + (.25 / 2), ],
[ 6.0 ],
];

maxRow = nestedMax(layout);
maxCol = len(layout) - 1;

width = swX(maxCol) + swX(1, 5) - 5;
height = swY(maxRow) + swX(1, 5) - 5;

module basePlate() {
	square(size=[width, height]);
}

module switchHoles() {
	for (col = [0 : len(layout) - 1]) {
		for (row = layout[col]) {
			swHole2D(col, row, true);
		}
	}
}

module plate() {
	mirror()
		difference() {
			basePlate();
			switchHoles();
		}
}

module switchBracing() {
	for (col = [0 : len(layout) - 1]) {
		x = swX(col) - 1.0;
		
		for (row = layout[col]) {
			y = swY(row) - 1.0;
			
			translate([x, y])
				square(size=[14 + 2, 14 + 2]);
		}
	}
}

module bracingPlate() {
	mirror() 
		difference() {
			basePlate();
			switchBracing();
		}
}

module rim() {
	mirror()
		difference() {
			basePlate();
			translate([3.5, 3.5])
				square(size=[width - 7, height - 7]);
	}
}

module centerSupport() {
	square(size=[15, 10]);
}

linear_extrude(height=1.5)
	plate();

color([1, 0, 0])
translate([0, 0, 1.5])
	linear_extrude(height=3.0)
		bracingPlate();
color([0, 0, 1])
	translate([-57, 121, 4.5])
		linear_extrude(height=8.0)
			centerSupport();

difference() {
	color([0, 1, 0])
		translate([0, 0, 4.5])
			linear_extrude(height=8.0)
				rim();

	translate([-5, 126, 11.0])
		color([1, 0, 1])
			rotate([0, 90, 0])
			cylinder(r=2, h=6, $fn=50);
}