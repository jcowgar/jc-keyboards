use <cherry-mx.scad>;
use <teensy.scad>;
use <utils.scad>;

include <JC33-config.scad>;

overallHeight = 11.0;

module squarePlate() {
	cube([plateWidth, plateDepth, plateHeight / 2.0]);
	reinforce(0, 0, plateWidth, plateDepth, overallHeight);
}

module rim() {
	minkowski() {
		reinforce(0, 0, plateWidth, plateDepth, overallHeight / 2.0, 0.5);
		cylinder(r=rounding, h=overallHeight / 2.0, $fn=50);
	}
}

module roundedPlate() {
	minkowski() {
		squarePlate();
		cylinder(r=rounding, h=plateHeight / 2.0, $fn=50);
	}
}

module switchHoles() {
	for (key = layout) {
		swHole(key[0], key[1], shouldReinforce=true);
	}
}
	
module plate() {
	difference() {
		squarePlate();
		//roundedPlate();
		switchHoles();
	}
}

module screwHole(r) {
	difference() {
		cylinder(r=r * 5, h=overallHeight, $fn=50);
		translate([0, 0, overallHeight - (overallHeight / 2)])
			cylinder(r=r - 0.1, h=overallHeight, $fn=50);
	}
}

module jc33() {
	plate();
	//rim();

	for (key = layout) {
		x = swX(key[1]) - 3.75;
		y = swY(key[0]) - 3.75;
		
		reinforce(y, x, 19 + 2.5, 19 + 2.5, 5.0);
	}

	translate([35, 0, 0])
		teensy20Holder(h=5.0);	

	for (sh = screwLocations) {
		translate([sh[0], sh[1], 0])
			screwHole(screwRadius);
	}
}

jc33();
