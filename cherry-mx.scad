use <utils.scad>;

plateThickness = 1.4;

function switchSpace(count) = (count * 19.0) + 5.0;

module cherryMxPlateMount2D(upDown=true, leftRight=false) {
	width = 14;
	height = 14;

	square(size=[width, height]);

	if (upDown) {
		translate([-0.8, 1])
			square(size=[0.9, 3.5]);
		translate([-0.8, height - 1 - 3.5])
			square(size=[0.9, 3.5]);

		translate([width - 0.1, 1])
			square(size=[0.9, 3.5]);
		translate([width - 0.1, height - 1 - 3.5])
			square(size=[0.9, 3.5]);
	}

	if (leftRight) {
		translate([1, -0.8])
			cube([3.5, 0.9]);
		translate([height - 1 - 3.5, -0.8])
			square(size=[3.5, 0.9]);

		translate([1, width - 0.1])
			square(size=[3.5, 0.9]);
		translate([height - 1 - 3.5, width - 0.1])
			square(size=[3.5, 0.9]);
	}
}

module cherryMxPlateMount(x, y, z, thickness, upDown=true, leftRight=false) {
	width = 14;
	height = 14;

	translate([x, y, z]) {
		cube([width, height, thickness]);

		if (upDown) {
			translate([-0.8, 1])
				cube([0.9, 3.5, thickness]);
			translate([-0.8, height - 1 - 3.5])
				cube([0.9, 3.5, thickness]);

			translate([width - 0.1, 1])
				cube([0.9, 3.5, thickness]);
			translate([width - 0.1, height - 1 - 3.5])
				cube([0.9, 3.5, thickness]);
		}

		if (leftRight) {
			translate([1, -0.8])
				cube([3.5, 0.9, thickness]);
			translate([height - 1 - 3.5, -0.8])
				cube([3.5, 0.9, thickness]);

			translate([1, width - 0.1])
				cube([3.5, 0.9, thickness]);
			translate([height - 1 - 3.5, width - 0.1])
				cube([3.5, 0.9, thickness]);
		}
	}
}

function switchHoleSize() = 14;
function switchPadding() = 5;
function switchTotalSize() = switchHoleSize() + switchPadding();

function swX(row) = (row * switchPadding()) + (row * 14) + switchPadding();
function swY(col) = (col * switchPadding()) + (col * 14) + switchPadding();

module swHole(col, row, upDown=false, leftRight=false) {
	startx = swX(col);
	starty = swY(row);

	cherryMxPlateMount(startx, starty, -plateThickness, plateThickness * 3.0, upDown, leftRight);
}

module swHole2D(col, row, upDown=false, leftRight=false) {
	startx = swX(col);
	starty = swY(row);

	translate([startx, starty])
		cherryMxPlateMount2D(upDown, leftRight);
}

swHole(0, 0);
