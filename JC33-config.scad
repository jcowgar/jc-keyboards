plateWidth = 214.0;
plateDepth = 90.5;
plateHeight = 1.5;
rounding = 2.5;

screwRadius = 1.0;
screwMargin = 5.0;
screwLocations = [
	[screwMargin, screwMargin],
	[plateWidth - screwMargin, screwMargin],
	[screwMargin, plateDepth - screwMargin],
	[plateWidth - screwMargin, plateDepth - screwMargin],
	[plateWidth / 2.0, 36.0],
];

layout = [
	// Left
	[0.00, 1.00], [0.00, 2.00], [0.00, 3.00],
	[1.00, 1.25], [1.00, 2.25], [1.00, 3.25],
	[2.25, 1.25], [2.00, 2.50], [2.00, 3.50],
	[3.25, 1.25], [3.00, 2.25], [3.00, 3.25],
	[4.25, 1.00], [4.00, 2.25], [4.00, 3.25],

	// Middle
	[5.00, 0.00], [5.00, 2.25], [5.00, 3.25],

	// Right
	[5.75, 1.00], [6.00, 2.25], [6.00, 3.25],
	[6.75, 1.25], [7.00, 2.25], [7.00, 3.25],
	[7.75, 1.25], [8.00, 2.50], [8.00, 3.50],
	[9.00, 1.25], [9.00, 2.25], [9.00, 3.25],
	[10.00, 1.00], [10.00, 2.00], [10.00, 3.00],
];

