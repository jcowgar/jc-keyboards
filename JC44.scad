use <cherry-mx.scad>;
use <teensy.scad>;
use <utils.scad>;

layout = [
	[ 0.50, 1.50, 2.50, 3.50 ], // Q
	[ 0.25, 1.25, 2.25, 3.25 ], // W
	[ 0.00, 1.00, 2.00, 3.00 ], // E
	[ 0.25, 1.25, 2.25, 3.25 ], // R
	[ 0.50, 1.50, 2.50, 3.50 ], // T
	[ 0.75, 1.75, 2.75, 4.50 ], // Blank/Shift
	[ 0.50, 1.50, 2.50, 3.50 ], // Y
	[ 0.25, 1.25, 2.25, 3.25 ], // U
	[ 0.00, 1.00, 2.00, 3.00 ], // I
	[ 0.25, 1.25, 2.25, 3.25 ], // O
	[ 0.50, 1.50, 2.50, 3.50 ], // P
];

maxRow = nestedMax(layout);
maxCol = len(layout) - 1;

width = swX(maxCol) + swX(1, 5) - 5;
height = swY(maxRow) + swX(1, 5) - 5;

module basePlate() {
	square(size=[width, height]);
}

module switchHoles() {
	for (col = [0 : len(layout) - 1]) {
		for (row = layout[col]) {
			swHole2D(col, row, true);
		}
	}
}

module plate() {
	difference() {
		basePlate();
		switchHoles();
	}
}

module switchBracing() {
	for (col = [0 : len(layout) - 1]) {
		x = swX(col) - 1.0;
		
		for (row = layout[col]) {
			y = swY(row) - 1.0;
			
			translate([x, y])
				square(size=[14 + 2, 14 + 2]);
		}
	}
}

module bracingPlate() {
	difference() {
		basePlate();
		switchBracing();
	}
}

module rim() {
	difference() {
		basePlate();
		translate([3.5, 3.5])
			square(size=[width - 7, height - 7]);
	}
}

linear_extrude(height=1.5)
	plate();

color([1, 0, 0])
translate([0, 0, 1.5])
	linear_extrude(height=3.0)
		bracingPlate();
color([0, 1, 0])
	translate([0, 0, 4.5])
		linear_extrude(height=8.0)
			rim();
