module reinforce(x, y, w, d, h, reinforceWidth=2.5) {
	translate([x, y]) {
		cube([w, reinforceWidth, h]);
		cube([reinforceWidth, d, h]);
	}
	
	translate([x + w - reinforceWidth, y])
		cube([reinforceWidth, d, h]);

	translate([x, y + d - reinforceWidth])
		cube([w - reinforceWidth, reinforceWidth, h]);
}

function nestedMax(v) = max([ for (row = v) max(row) ]);
