module teensy20Holder(h=5.0, borderWidth=2.5) {
	marginOfError = 0.2;
	teensyWidth = 17.8 + marginOfError;
	teensyLength = 30.5 + marginOfError;
	
	cube([borderWidth, teensyWidth + borderWidth, h]);
	cube([teensyLength + borderWidth, borderWidth, h]);
	translate([0, teensyWidth + borderWidth, 0])
		cube([teensyLength + borderWidth, borderWidth, h]);
	translate([teensyLength + borderWidth, 0, 0])
		cube([borderWidth, borderWidth + 2.0, h]);
	translate([teensyLength + borderWidth, teensyWidth + borderWidth - 2.0, 0])
		cube([borderWidth, borderWidth + 2.0, h]);
}

teensy20Holder(h=2.0, borderWidth=1.0);
