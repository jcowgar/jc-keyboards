include <JC33-config.scad>;

$fn=50;

module plate() {
	minkowski() {
		cube([plateWidth, plateDepth, thickness]);
		cylinder(r=rounding, h=1.0);
	}
}

module bottomPlate() {
	thickness = 1.0;
	
	difference() {
		plate();
		translate([plateWidth / 2.0, plateDepth / 2.0, -thickness * 2])
			linear_extrude(thickness * 6.0)
				text("JC33", 62, font="Fira Code Medium", $fn=50, valign="center", halign="center");

		for (sh = screwLocations) {
			translate([sh[0], sh[1], -plateDepth])
				cylinder(r=screwRadius + 0.1, h=plateDepth * 3.0);
		}
	}
}

bottomPlate();
