plateThickness = 1.4;

module cherryMxPlateMount(x, y, z, thickness, upDown=true, leftRight=false) {
	width = 14;
	height = 14;
	
	translate([x, y, z]) {
		cube([width, height, thickness]);
		
		if (upDown) {
			translate([-0.8, 1])
				cube([0.9, 3.5, thickness]);
			translate([-0.8, height - 1 - 3.5])
				cube([0.9, 3.5, thickness]);

			translate([width - 0.1, 1])
				cube([0.9, 3.5, thickness]);
			translate([width - 0.1, height - 1 - 3.5])
				cube([0.9, 3.5, thickness]);
		}

		if (leftRight) {
			translate([1, -0.8])
				cube([3.5, 0.9, thickness]);
			translate([height - 1 - 3.5, -0.8])
				cube([3.5, 0.9, thickness]);

			translate([1, width - 0.1])
				cube([3.5, 0.9, thickness]);
			translate([height - 1 - 3.5, width - 0.1])
				cube([3.5, 0.9, thickness]);
		}
	}
}

module swHole(col, row, upDown=false, leftRight=false) {
	startx = (col * 5) + (col * 14) + 5;
	starty = (row * 5) + (row * 14) + 5;
	
	cherryMxPlateMount(startx, starty, -plateThickness, plateThickness * 3.0, upDown, leftRight);
}

difference() {
	cube([24,24, plateThickness]);
	swHole(0, 0);
}

cube([22,2,6]);
cube([2,22,6]);
translate([0,22])
	cube([22,2,6]);
translate([22,0])
	cube([2,24,6]);