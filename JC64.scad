use <cherry-mx.scad>;
use <teensy.scad>;
use <utils.scad>;

layout = [
	[ 0.00, 1.00, 2.00, 3.00, 4.00, 5.00, 6.00 ],
	[ 0.00, 1.00, 2.00, 3.00, 4.00, 5.00, 6.00 ],
	[ 0.00, 1.00, 2.00, 3.00, 4.00, 5.00, 6.00 ],
	[ 0.00 + (1.0 / 2), 2.00, 3.00, 4.00, 5.00, 6.00 ],
	[ 1.25 + (0.25 / 2), 2.5 + (.25 / 2), 4.00, 5 + (.75 / 2)],
];

maxRow = nestedMax(layout);
maxCol = len(layout) - 1;

width = swX(maxCol) + swX(1, 5) - 5;
height = swY(maxRow) + swX(1, 5) - 5;

module centerPlate() {
	centerHeight = 43;
	centerWidth = width - 4;
	
	difference() {
		polygon([[0, 0], [centerHeight / 2.0, centerWidth], [centerHeight, 0]]);
		swHole2D(0.5, 1);
		swHole2D(0.5, 0);
	}
}

module centerReinforcement() {
	difference() {
		centerPlate();
		
		translate([swX(0.5) - 1.0, swY(1) - 1.0, 0]) square(size=[14 + 2, 14 + 2]);
		translate([swX(0.5) - 1.0, swY(0) - 1.0, 0]) square(size=[14 + 2, 14 + 2]);
	}
}

module basePlate() {
	square(size=[width, height]);
}

module switchHoles() {
	for (col = [0 : len(layout) - 1]) {
		for (row = layout[col]) {
			swHole2D(col, row);
		}
	}
}

module plate() {
	difference() {
		basePlate();
		switchHoles();
	}
}

module leftPlate() {
	mirror()
		plate();
}

module switchBracing() {
	for (col = [0 : len(layout) - 1]) {
		x = swX(col) - 1.0;
		
		for (row = layout[col]) {
			y = swY(row) - 1.0;
			
			translate([x, y])
				square(size=[14 + 2, 14 + 2]);
		}
	}
}

module bracingPlate() {
	difference() {
		basePlate();
		switchBracing();
	}
}

module leftBracingPlate() {
	mirror() bracingPlate();
}

module rim() {
	difference() {
		basePlate();
		translate([3.5, 3.5])
			square(size=[width - 7, height - 7]);
	}
}

module leftRim() {
	mirror() rim();
}

module left() {
	linear_extrude(height=1.5)
		leftPlate();

	color([1, 0, 0])
	translate([0, 0, 1.5])
		linear_extrude(height=3.0)
			leftBracingPlate();

	difference() {
		color([0, 1, 0])
			translate([0, 0, 4.5])
				linear_extrude(height=8.0)
					leftRim();

		translate([-5, 126, 11.0])
			color([1, 0, 1])
				rotate([0, 90, 0])
				cylinder(r=2, h=6, $fn=50);
	}
}

module right() {
	linear_extrude(height=1.5)
		plate();

	color([1, 0, 0])
	translate([0, 0, 1.5])
		linear_extrude(height=3.0)
			bracingPlate();

	difference() {
		color([0, 1, 0])
			translate([0, 0, 4.5])
				linear_extrude(height=8.0)
					rim();

		translate([-5, 126, 11.0])
			color([1, 0, 1])
				rotate([0, 90, 0])
				cylinder(r=2, h=6, $fn=50);
	}
}

module center() {
	linear_extrude(height=1.5) centerPlate();
}

module cReinforcement() {
	linear_extrude(height=3.5) centerReinforcement();
}

module keyboard() {
	rotate([0, 0, 12]) left();
	translate([0, (height * 2) - 7, 0]) rotate([0, 0, 168]) right();
	translate([-126.4, 156, 0]) rotate([0, 0, 270]) color([0, 0.8, 0.8]) center();
	translate([-126.4, 156, 1.5]) rotate([0, 0, 270]) color([0.8, 0.8, 0]) cReinforcement();
}

difference() {
	keyboard();
	color([0.5, 0.5, 0]) translate([-81.5, 110, 4.6]) cube([50.0, 50.0, 8.0]);
}
